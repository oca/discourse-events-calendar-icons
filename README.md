This theme component requires :
* The [Category icons component](https://meta.discourse.org/t/category-icons-component/104683)
* The [Events plugin](https://meta.discourse.org/t/events-plugin/69776)


After installation, you need to modify slightly the code in the Category icons component.
* Go to `https://myforum.tld/admin/customize/themes/` and choose: Category Icons
* Click: Modify CSS/HTML and Ok the warning
* Select: Header and look for: `api.createWidget("category-icon", {` near the end of the file.
* Replace the method by :
  ```js
  api.createWidget("category-icon", {
    tagName: "div.category-icon-widget",
    html(attrs) {
      let iconItem = getIconItem(attrs.category.slug);
      let force_display = ('force_display' in attrs);
      if(iconItem && (!attrs.category.parent_category_id || force_display)) {
        let itemColor = iconItem[2] ? `color: ${iconItem[2]}` : "";
        let itemIcon = iconItem[1] != '' ? iconNode(iconItem[1]) : "";
        return h("span.category-icon", { "style": itemColor }, itemIcon);
      }
    }
  });
  ```

This change add a way to force the icon to be displayed even if it is not from a top level category. If you know a way to acheive this goal without those steps, please let me know.
